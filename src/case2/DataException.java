package case2;

public class DataException extends Exception {
	
	public DataException() {
		super();
	}
	
	public DataException(String a) {
		super(a);
	}

}
