package case3;

public class FullException extends Exception {
	
	public FullException() {
		super();
	}
	
	public FullException(String a) {
		super(a);
	}

}
