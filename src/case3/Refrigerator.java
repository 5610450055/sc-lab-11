package case3;

import java.util.ArrayList;

public class Refrigerator {
	
	private int size;
	ArrayList<String> things;
	
	public Refrigerator(int size) {
		this.size=size;
		things = new ArrayList<String>();
	}
	
	public void put(String stuff) throws FullException {
		if (things.size()>=size) {
			throw new FullException("Refrigerator space is full");
		}
		things.add(stuff);
	}
	
	public String takeOut(String stuff) {
		if (things.isEmpty()) {
			return "\nNot found "+stuff+": "+null+"\n";
		}
		for (int i=0; i<things.size(); i++) {
			if (things.get(i).equals(stuff)) {
				things.remove(i);
				return "\nFinding result is: "+stuff+"\nTakeOut: "+stuff+"\n";
			}
		}
		return "\nNot found "+stuff+": "+null+"\n";
		
	}
	
	public String toString() {
		if(things.isEmpty()){
			return "\nRefrigerator is empty";
		}
		String total="";
		for (String s : things) {
			total +=s+" ";
		}
		return "\nList of item: "+total+"\n";
	}

}
