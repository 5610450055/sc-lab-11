package case3;

public class Main {
	
	public static void main(String[] args) {
		Refrigerator r = new Refrigerator(5);
		try {
			r.put("Apple");
			r.put("Broccoli");
			r.put("Beef");
			r.put("Pork");
			r.put("Tomato");
			System.out.print(r.toString());
			System.out.print(r.takeOut("Apple"));
			r.put("OrangeJuice");
			r.put("Icecream");
			
		}catch (FullException e) {
			System.err.print("\nError: "+e.getMessage()+"\n");
		}
		finally {
			System.out.print(r.toString());
			System.out.print(r.takeOut("Banana"));
		}
	}

}
