package case1;

import java.util.HashMap;

public class WordCounter {
	
	private String message;
	private HashMap<String,Integer> wordCount;
	
	public WordCounter(String message) {
		this.message=message;
	}
	
	public void count() {
		String[] listA = message.split(" ");
		wordCount = new HashMap<String, Integer>();
		
		for (int i=0; i<listA.length; i++) {
            if (!wordCount.containsKey(listA[i])) {
            	wordCount.put(listA[i], 1);
            } else {
            	wordCount.put(listA[i], (Integer) wordCount.get(listA[i]) + 1);
            }
        }
	}
	
	public int hasWord(String word) {
		int count=0;
		for (Object x : wordCount.keySet()){
			if (x.equals(word)) {
				count=wordCount.get(x);
			}
        }
		return count;
	}

}
